<?php

use Illuminate\Database\Seeder;
use App\SubCategory;
use App\MainCategory;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Посев категорий и подкатегорий
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Транспорт' => [
                'Легковые автомобили',
                'Грузовые автомобили',
                'Мото',
                'Автобусы',
                'Другой транспорт'
            ],
            'Недвижимость' => [
                'Квартиры',
                'Дома',
                'Земля',
                'Другая недвижимость'
            ],
            'Дом и сад' => [
                'Мебель',
                'Инструменты',
                'Стоительство',
                'Другое в дом и сад'
            ],
            'Мода и стиль' => [
                'Одежда',
                'Обувь',
                'Аксесуары',
                'Другое в мода и стиль'
            ],
            'Электроника' => [
                'Телефоны',
                'Компьютеры',
                'ТВ',
                'Другая электроника'
        ]];

        foreach ($categories as $main => $subCategories) {
            $mainCategory = MainCategory::create([
                        'name' => $main
            ]);
            foreach ($subCategories as $subCategory) {
                SubCategory::create([
                    'name' => $subCategory,
                    'main_category_id' => $mainCategory->id
                ]);
            }
        }
    }
}
