<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\IMainCategory;

class MainCategory extends Model implements IMainCategory
{
    /**
     * Определяет необходимость отметок времени для модели.
     *
     * @var bool
     */
    public $timestamps = false;
    
    protected $table = 'main_categories';
    
    protected $fillable = ['name'];
    
    /*
     * Связб с подкатегориями главной категории
     */
    public function getCategories()//: iterable
    {
        return $this->hasMany('App\SubCategory', 'main_category_id', 'id');
    }
}
