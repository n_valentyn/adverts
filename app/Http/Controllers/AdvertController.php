<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Addition\Session\SessionInerface;
use App\Advert;
use App\Favorite;
use App\Metric;
use App\Comment;
use App\User;
use Storage;

/*
 * Контроллер по работе с обьявлениями
 */
class AdvertController extends Controller
{
    /**
     * Создание обьявления
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Responce
     */
    public function creatAction(Request $request, SessionInerface $session)
    {
        
        //Запись переданых даных
        $advert = new Advert;
        $advert->title = $request->title;
        $advert->description = $request->description;
        $advert->price = $request->price;
        $advert->category_id = $request->categoryId;
        $advert->status = true;
        $advert->user_id = $session->getUserId();
        
        //Если передано изображение, сохранение его
        if ($request->hasFile('image')) {
            $advert->image = $request->file('image')->store('', 'images');
        }
        //Сохранение объявления
        $advert->save();
        
        // Ответ об успешном создании обявления
        return response()->tempJson(200, $advert->getInfo());
    }
    
    /**
     * Получение обьявления по переданому id
     *
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request)
    {
        //Поиск обьявления
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдего
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об отсутствии обьявления
            return response()->tempJson(404, [], $errors);
        }
        
        //Возвращает обьявление
        return response()->tempJson(200, $advert->getInfo());
    }
    
    /**
     * Получение всех активных обьявлений
     *
     * @return Response
     */
    public function getAllAction()
    {
        //Поиск всех активных обьявлений и получение информции в массиве
        $adverts = [];
        foreach (Advert::where('status', true)->get() as $advert) {
            $adverts[] = $advert->getInfo();
        }
        
        //Возвращает список обьявлений
        return response()->tempJson(200, $adverts);
    }
    
    /**
     * Поиск и сортировка обьявлений по переданому фильтру
     *
     * @param Request $request
     * @return Responce
     */
    public function getFilterAction(Request $request)
    {
        //Подготовка фильтра, удаление пустых элементов
        $filter = array_filter($request->all());
        
        //Подготовка данных на возврат
        //Запись фильтра в ответ,
        //Поиск обьявлений удовлетворяющих фильтр
        $data = [
            'filter' => $filter,
            'adverts' => Advert::findByFilter($filter)
        ];
        
        //Ответ
        return response()->tempJson(200, $data);
    }
    
    /**
     * Получение избранных обьявлений пользователя
     *
     * @param SessionInerface $session
     * @return Response
     */
    public function getFavoriteAction(SessionInerface $session)
    {
        //Получение пользователя
        $user = User::find($session->getUserId());
        
        //Получение и запись в виде массива обьявлений пользователя
        $adverts = [];
        foreach ($user->getFavoriteAdverts as $advert) {
            $adverts[] = $advert->getInfo();
        }
        
        //Ответ
        return response()->tempJson(200, $adverts);
    }
    
    /**
     * Обновление данных в обьявлении
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function updateAction(Request $request, SessionInerface $session)
    {
        // Поля которые нужны для обновления данных
        $filds = ['title', 'description', 'price', 'categoryId', 'status'];

        // Получение массива нужных полей и удаления пустих елементов
        $data = array_filter($request->only($filds));
        
        //Если массив полей пустой
        if (count($data) == 0) {
            $errors[] = 'No data for updating';
            
            // Ответ об отсутствии данных для обновления
            return response()->tempJson(422, $data, $errors);
        }
        
        //Поиск обьявления которое нужно обновить
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, $data, $errors);
        }
        
        //Если пользователь не является автором обьявления
        if ($advert->user_id != $session->getUserId()) {
            $errors[] = 'No access rights to this advert';
            //Ответ об отсутствии прав доступа
            return response()->tempJson(401, [], $errors);
        }
        
        //Перевод ключей массива в snake_case
        foreach ($data as $key => $value) {
            $advertData[snake_case($key)] = $value;
        }
        //Если есть изображение для обновления
        if ($request->hasFile('image')) {
            //Удаление старого изображения
            Storage::disk('images')->delete($advert->image);
            
            //Сохранение нового изображения
            $advertData['image'] = $request->file('image')->store('', 'images');
        }
        
        //Обновление данных в обьявлении
        $advert->fill($advertData)->save();

        // Ответ об успешном обновлении объявления
        return response()->tempJson(200, $advert->getInfo());
    }
    
    /**
     * Удаление обьявления
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function deleteAction(Request $request, SessionInerface $session)
    {
        //Поиск обьявления которое нужно удалить
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, [], $errors);
        }
        
        //Если пользователь не является автором обьявления
        if ($advert->user_id != $session->getUserId()) {
            $errors[] = 'No access rights to this advert';
            //Ответ об отсутствии прав доступа
            return response()->tempJson(401, [], $errors);
        }
        
        //Удаление
        $advert->delete();
       
        //Ответ
        return response()->tempJson(200);
    }
    
    /**
     * Добавление обьявления в избранное пользователя
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function addFavoriteAction(Request $request, SessionInerface $session)
    {
        //Поиск обьявления которое нужно добавить в избранное
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, [], $errors);
        }
        
        //Поиск обьявления в избранных пользователя
        $favorite = Favorite::where('user_id', $session->getUserId())
                            ->where('advert_id', $request->route('advert'))
                            ->first();
        
        //Если обьявления найдено в избранных
        if ($favorite != null) {
            $errors[] = 'Advert already added to favorites';
            //Ответ о том что обьявление уже присутствует в избранных
            return response()->tempJson(409, [], $errors);
        }
        
        //Добавление в избранное
        Favorite::create([
            'user_id' => $session->getUserId(),
            'advert_id' => $request->route('advert')
        ]);
       
        //Ответ
        return response()->tempJson(200);
    }
    
    /**
     * Удаление обьявления из избранных
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function deleteFavoriteAction(Request $request, SessionInerface $session)
    {
        //Поиск обьявления которое нужно удалить из избранных
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, [], $errors);
        }
        
        //Поиск обьявления в избранных пользователя
        $favorite = Favorite::where('user_id', $session->getUserId())
                            ->where('advert_id', $request->route('advert'))
                            ->first();
        
        //Если бьявление не найдено в избранных
        if ($favorite == null) {
            $errors[] = 'Advert not found in favorites';
            //Ответ что обьявления не найдено в избранных
            return response()->tempJson(409, [], $errors);
        }
        
        //Удаление обьявления из избранных
        $favorite->delete();
       
        return response()->tempJson(200);
    }
    
    /**
     * Добавление метрики обьявления
     *
     * @param Request $request
     * @return Response
     */
    public function addMetricsAction(Request $request)
    {
        // Поля которые нужны для добавления метрики
        $filds = ['ipAddress', 'referer', 'userAgent'];

        // Получение массива нужных полей и удаления пустих елементов
        $data = array_filter($request->only($filds));
        
        //Если массив полей пустой
        if (count($data) == 0) {
            $errors[] = 'No data for adding';
            
            // Ответ об отсутствии данных для добавления
            return response()->tempJson(422, $data, $errors);
        }
        
        //Поиск обьявления к которому нужно добавить метрику
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, $request->all(), $errors);
        }
        
        //Перевод ключей полей метрики в snake_case
        foreach ($data as $key => $value) {
            $metricData[snake_case($key)] = $value;
        }
        
        //Добавление объявления к которому относится метрика
        $metricData['advert_id'] = $request->route('advert');
        
        //Создание метрики
        $metric = Metric::create($metricData);
        
        //Ответ
        return response()->tempJson(200, $metric->getInfo());
    }
    
    /**
     * Получение списка метрик объявления
     *
     * @param Request $request
     * @return Response
     */
    public function getMetricsAction(Request $request)
    {
        //Поиск обьявления по которому нужно получить метрику
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, $request->all(), $errors);
        }
        
        //Получение метрики
        $metrics = [];
        foreach ($advert->getMetrics as $metric) {
            $metrics[] = $metric->getInfo();
        }
        
        //Ответ
        return response()->tempJson(200, $metrics);
    }
    
    /**
     * Добавление комментария к объявлению
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function addCommentAction(Request $request, SessionInerface $session)
    {
        //Поиск обьявления к которому нужно добавить комментарий
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, $request->all(), $errors);
        }
        
        //Создание и сохранение комментария
        $comment = new Comment;
        $comment->text = $request->text;
        $comment->user_id = $session->getUserId();
        $comment->advert_id = $advert->id;
        $comment->save();
        
        //Ответ
        return response()->tempJson(200, $comment->getInfo());
    }
    
    /**
     * Получение комментариев объявления
     *
     * @param Request $request
     * @return Response
     */
    public function getCommentsAction(Request $request)
    {
        //Поиск обьявления по которому нужно получить комментарии
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, $request->all(), $errors);
        }
        
        //Получение комментариев
        $comments = [];
        foreach ($advert->getComments as $comment) {
            $comments[] = $comment->getInfo();
        }
        
        //Ответ
        return response()->tempJson(200, $comments);
    }
    
    /**
     * Удаление комментария объявления
     *
     * @param Request $request
     * @param SessionInerface $session
     * @return Response
     */
    public function deleteCommentAction(Request $request, SessionInerface $session)
    {
        //Поиск обьявления по которому нужно удалить комментарий
        $advert = Advert::find($request->route('advert'));
        
        //Если обьявление не найдено
        if ($advert == null) {
            $errors[] = 'Advert not found';
            //Ответ об ошибке
            return response()->tempJson(404, [], $errors);
        }
        
        //Поиск комментария который нужно удалить
        $comment = Comment::find($request->route('comment'));
        
        //Если комментарий не найден
        if ($comment == null) {
            $errors[] = 'Comment not found';
            //Ответ об ошибке
            return response()->tempJson(404, [], $errors);
        }
        
        //Если пользователь не является автором комментария
        if ($comment->user_id != $session->getUserId()) {
            $errors[] = 'No access rights to this comment';
            //Ответ об отсутствии прав доступа
            return response()->tempJson(401, [], $errors);
        }
        
        //Если комментарий не принадлежит текущему объявлению
        if ($comment->advert_id != $request->route('advert')) {
            $errors[] = 'Comment does not apply to the current advert';
            //Ответ об Ошибке
            return response()->tempJson(409, [], $errors);
        }
        
        //Удаление комментария
        $comment->delete();
       
        //Ответ
        return response()->tempJson(200);
    }
    
    /**
     * Возвращает изображение по имени
     *
     * @param string $image имя изображение
     * @return Response
     */
    public function getImageAction($image)
    {
        //Если на диску есть изображение
        if (Storage::disk('images')->has($image)) {
            //Получение изображения
            $file = Storage::disk('images')->get($image);
            //Получение MIME тип изображения
            $type = Storage::disk('images')->mimeType($image);
            //Ответ
            return response($file, 200)->header('Content-Type', $type);
        }
        return response('', 404);
    }
}
