<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MainCategory;

/*
 * Контроллер категорий обьявлений
 */
class CategoriesController extends Controller
{
    /**
     * Возвращает список главных категорий
     *
     * @return Response
     */
    public function getAction()
    {
        //Получение списка категорий
        $categories = MainCategory::all()->toArray();
        
        //Ответ
        return response()->tempJson(200, $categories);
    }
    
    /**
     * Возвращает список подкатегорий по переданой главной категории
     *
     * @param Request $request
     * @return Response
     */
    public function getSubAction(Request $request)
    {
        //Получение главной категории
        $category = MainCategory::find($request->route('category'));
        
        //Если категория не найдена
        if ($category == null) {
            $errors[] = 'Category not found';
            //Ответ об отсутствии такой категории
            return response()->tempJson(404, [], $errors);
        }
        
        //Ответ список подкатегорий
        return response()->tempJson(200, $category->getCategories->toArray());
    }
}
