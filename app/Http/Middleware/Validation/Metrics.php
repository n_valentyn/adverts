<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\ValidationAbstract;

class Metrics extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'ipAddress' => 'sometimes|nullable|ip',
            'referer' => 'sometimes|nullable|string|min:3|max:511',
            'userAgent' => 'sometimes|nullable|string|min:3|max:511',
        ];
    }
}
