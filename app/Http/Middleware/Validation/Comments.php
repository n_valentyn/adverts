<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\ValidationAbstract;

class Comments extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'x-access-token' => 'required|string|min:50|max:255',
            'text' => 'required|string|min:3|max:1023'
        ];
    }
}
