<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\ValidationAbstract;

class UpdateAdvert extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'x-access-token' => 'required|string|min:50|max:255',
            'title' => 'sometimes|nullable|string|min:5|max:255',
            'description' => 'sometimes|nullable|string|min:5|max:2048',
            'price' => 'sometimes|nullable|numeric|max:1000000000',
            'categoryId' => 'sometimes|nullable|integer|exists:sub_categories,id',
            'status' => 'sometimes|nullable|in:true,false',
            'image' => 'sometimes|nullable|image|max:1024',
        ];
    }
}
