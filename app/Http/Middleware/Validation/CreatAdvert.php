<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\ValidationAbstract;

class CreatAdvert extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'x-access-token' => 'required|string|min:50|max:255',
            'title' => 'required|string|min:5|max:255',
            'description' => 'required|string|min:5|max:2048',
            'price' => 'required|numeric|max:1000000000',
            'categoryId' => 'required|integer|exists:sub_categories,id',
            'image' => 'sometimes|nullable|image|max:1024'
        ];
    }
}
