<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\ValidationAbstract;

class Search extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'search' => 'sometimes|nullable|string|max:127',
            'userId' => 'sometimes|nullable|integer|exists:users,id',
            'categoryId' => 'sometimes|nullable|integer|exists:sub_categories,id',
            'priceUp' => 'sometimes|nullable|numeric|max:1000000000',
            'priceDown' => 'sometimes|nullable|numeric|max:1000000000',
            'status' => 'sometimes|nullable|in:true,false',
            'orderBy' => 'sometimes|nullable|in:createdAt,updatedAt,price,title',
            'sort' => 'sometimes|nullable|in:asc,desc',
        ];
    }
}
