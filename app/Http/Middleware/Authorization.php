<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use App\Addition\Session\SessionInerface;

/**
 *
 * Посредник проверки авторизации
 */
class Authorization
{
    /*
     * Сессия
     * var SessionInerface
     */
    private $session;
    
    /*
     * Создание сессии при проверки авторизации
     */
    public function __construct(SessionInerface $session)
    {
        $this->session = $session;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Запись переданых данных в массив
        $data = array_merge(
                ['x-access-token' => $request->header('x-access-token')],
                $request->all()
        );
        
        //Создание клиента з URI сервиса авторизации
        $client = new Client(['base_uri' => $_ENV['AUTH_SERVICE']]);
        
        //Запись в параметры токена
        $options['headers']['x-access-token'] = $request->header('x-access-token');
        
        //Отключить исключения при ошибках протокола HTTP (Ответы 4xx и 5xx).
        $options['http_errors'] = false;
        
        //Запрос на получение сессии
        $res = $client->request('GET', 'session', $options);
         
        //Если есть ошибки сервиса авторизации
        if ($res->getStatusCode() != 200) {
            $errors[] = 'Authorization service error ' . $res->getStatusCode();
            //Ответ об ошибке
            return response()->tempJson(503, $data, $errors);
        }
        
        //Установка сессии по данным сервиса авторизации
        $this->session->set($res->getBody());
        
        //Если сессии не получена
        if ($this->session->getStatus() == false) {
            //Ответ с ошибками сервиса авторизации
            return response()->tempJson(401, $data, $this->session->getErrors());
        }
        
        return $next($request);
    }
}
