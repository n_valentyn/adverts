<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\MetricInterface;

class Metric extends Model implements MetricInterface
{
    protected $table = 'metrics';
    
    protected $fillable = ['ip_address', 'referer', 'user_agent', 'advert_id'];
    
    protected $hidden = ['advert_id', 'updated_at','id'];
    
    /**
     * Возвращает метрику в виде массива
     *
     * @return array
     */
    public function getInfo(): array
    {
        $metricBD = $this->toArray();
        
        foreach ($metricBD as $key => $value) {
            $metric[camel_case($key)] = $value;
        }
        
        return $metric;
    }
}
