<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'city'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'password_token',
    ];
    /*
     * Связь с избраннымы объявлениями пользователя
     */
    public function getFavoriteAdverts()
    {
        return $this->belongsToMany('App\Advert', 'favorites', 'user_id', 'advert_id');
    }
}
