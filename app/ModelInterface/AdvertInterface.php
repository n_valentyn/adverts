<?php

namespace App\ModelInterface;

use App\ModelInterface\IMainCategory;
use App\ModelInterface\ISubCategory;

/**
 *
 * @author valentyn
 */
interface AdvertInterface
{
    public function getInfo(): array;
        
    public function getMetrics();//: iterable;
        
    public function getComments();//: iterable;
        
    public function getRootCategory();//: IMainCategory;
        
    public function getCategory();//: ISubCategory;
        
    public function getCountInFavorite(): int;
        
    public function getAuthor();
        
    public static function findByFilter(array $filter): array;
}
