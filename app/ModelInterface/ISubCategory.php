<?php

namespace App\ModelInterface;

use App\ModelInterface\IMainCategory;

/**
 *
 * @author valentyn
 */
interface ISubCategory
{
    public function getMainCategory();//: IMainCategory;
    
    public function getAdverts();//: iterable;
}
