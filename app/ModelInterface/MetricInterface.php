<?php

namespace App\ModelInterface;

/**
 *
 * @author valentyn
 */
interface MetricInterface
{
    public function getInfo(): array;
}
