<?php

namespace App\ModelInterface;

use App\ModelInterface\AdvertInterface;

/**
 *
 * @author valentyn
 */
interface CommentInterface
{
    public function getInfo(): array;
        
    public function getAuthor();//: UserInterface;
        
    public function getAdvert();//: AdvertInterface;
}
