<?php

namespace App\ModelInterface;

/**
 *
 * @author valentyn
 */
interface IMainCategory
{
    public function getCategories();//: iterable;
}
