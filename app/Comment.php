<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\CommentInterface;

class Comment extends Model implements CommentInterface
{
    protected $table = 'comments';
    
    protected $fillable = ['text', 'advert_id', 'user_id'];
    
    /*
     * Связь с объявлением
     */
    public function getAdvert()
    {
        return $this->belongsTo('App\Advert', 'advert_id', 'id');
    }
    
    /*
     * Связь с Автором
     */
    public function getAuthor()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Получение комментария в виде массива
     *
     * @return array
     */
    public function getInfo(): array
    {
        $data['id'] = $this->id;
        $data['text'] = $this->text;
        $data['createdAt'] = $this->created_at->__toString();
        
        $data['author']['id'] = $this->user_id;
        $data['author']['firstName'] = $this->getAuthor->first_name;
        $data['author']['lastName'] =  $this->getAuthor->last_name;
        
        return $data;
    }
}
