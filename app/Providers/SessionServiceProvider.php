<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Addition\Session\SessionInerface;
use \App\Addition\Session\Session;

class SessionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SessionInerface::class, function ($app) {
            return new Session();
        });
    }
}
