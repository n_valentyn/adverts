<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\AdvertInterface;
use App\ModelInterface\ISubCategory;
use App\ModelInterface\IMainCategory;

//Модель объявления
class Advert extends Model implements AdvertInterface
{
    protected $fillable = ['title', 'description', 'price', 'category_id', 'status'];

    /**
     * Поиск обявлений по фильтру
     *
     * @param array $filter
     * @return array
     */
    public static function findByFilter(array $filter): array
    {
        $where = [];
        //Если в фильтре есть поиск в названиях объявлений
        if (array_key_exists('search', $filter)) {
            //Разбывка строки на слова
            $search = explode(' ', $filter['search']);

            //Добавление слов в условие поиска
            foreach ($search as $word) {
                if ($word != '') {
                    $where[] = ['title', 'ILIKE', '%' . $word . '%'];
                }
            }
        }

        //Если в фильтре есть поиск по пользователю
        if (array_key_exists('userId', $filter)) {
            //Добаление id пользователя в условие поиска
            $where[] = ['user_id', $filter['userId']];
        }

        //Если в фильтре есть поиск по категории
        if (array_key_exists('categoryId', $filter)) {
            //Добаление id категории в условие поиска
            $where[] = ['category_id', $filter['categoryId']];
        }

        //Если в фильтре есть поиск по стоимости больше чем
        if (array_key_exists('priceUp', $filter)) {
            //Добавление условия больше чем
            $where[] = ['price', '>=', $filter['priceUp']];
        }

        //Если в фильтре есть поиск по стоимости меньше чем
        if (array_key_exists('priceDown', $filter)) {
            //Добавление условия больше чем
            $where[] = ['price', '<=', $filter['priceDown']];
        }
        
        //Если в фильтре есть поиск по статусу
        if (array_key_exists('status', $filter)) {
            //Добавление условия статуса
            $where[] = ['status', $filter['status']];
        }

        //Если в фильтре есть сортировка по столбцу
        if (array_key_exists('orderBy', $filter)) {
            //Запись столбца по которому нужно сортировать
            $orderBy = snake_case($filter['orderBy']);
        } else {
            //Иначе установка по умолчанию дата создания объявлени
            $orderBy = 'created_at';
        }

        //Если в фильтре есть направление сортировки
        if (array_key_exists('sort', $filter)) {
            //Запись направление сортировки
            $sort = $filter['sort'];
        } else {
            //Иначе по умолчанию сортировка по убыванию
            $sort = 'desc';
        }

        //Получение объявлений по условиях
        $adverts = [];
        foreach (Advert::where($where)->orderBy($orderBy, $sort)->get() as $advert) {
            $adverts[] = $advert->getInfo();
        }

        return $adverts;
    }

    /**
     * Связь с Автором объявления
     */
    public function getAuthor()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Связь с категорией объявления
     */
    public function getCategory()//: ISubCategory
    {
        return $this->belongsTo('App\SubCategory', 'category_id', 'id');
    }

    /**
     * Связь с комментариями объявления
     */
    public function getComments()//: iterable
    {
        return $this->hasMany('App\Comment', 'advert_id', 'id');
    }

    /**
     * Получение количества в избранных
     *
     * @return int
     */
    public function getCountInFavorite(): int
    {
        return $this->belongsToMany('App\User', 'favorites', 'advert_id', 'user_id')->count();
    }

    /**
     * Получение объявления в виде массива
     *
     * @return array
     */
    public function getInfo(): array
    {
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['description'] = $this->description;
        $data['price'] = $this->price;
        $data['status'] = $this->status;
        
        $data['image'] = null;
        if ($this->image) {
            $data['image'] = route('advert.image', ['image' => $this->image]);
        }
        
        $data['createdAt'] = $this->created_at->__toString();
        $data['updatedAt'] = $this->updated_at->__toString();
        $data['CountInFavorite'] = $this->getCountInFavorite();
        
        $data['author']['id'] = $this->user_id;
        $data['author']['firstName'] = $this->getAuthor->first_name;
        $data['author']['lastName'] = $this->getAuthor->last_name;
        $data['author']['city'] = $this->getAuthor->city;

        $data['category']['id'] = $this->category_id;
        $data['category']['name'] = $this->getCategory->name;

        $data['rootCategory']['id'] = $this->getRootCategory()->id;
        $data['rootCategory']['name'] = $this->getRootCategory()->name;

        return $data;
    }

    /**
     * Связь с метриками объявления
     */
    public function getMetrics()//: iterable
    {
        return $this->hasMany('App\Metric', 'advert_id', 'id');
    }

    /**
     * Связь с главной категорией объявления
     */
    public function getRootCategory()//: IMainCategory
    {
        return $this->getCategory->getMainCategory;
    }
}
