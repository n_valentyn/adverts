<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\ISubCategory;
use App\ModelInterface\IMainCategory;

class SubCategory extends Model implements ISubCategory
{
    public $timestamps = false;
    
    protected $table = 'sub_categories';
    
    protected $fillable = ['name', 'main_category_id'];
    
    protected $hidden = ['main_category_id'];
    
    /*
     * Связь с Объявлениями категории
     */
    public function getAdverts()//: iterable
    {
        return $this->hasMany('App\Advert', 'category_id', 'id');
    }

    /**
     * Связь с главной категорией
     */
    public function getMainCategory()//: IMainCategory
    {
        return $this->belongsTo('App\MainCategory', 'main_category_id', 'id');
    }
}
