<?php

namespace App\Addition\Session;

use App\Addition\Session\SessionInerface;

/**
 * Description of Session
 *
 * @author valentyn
 */
class Session implements SessionInerface
{
    private $token;
    
    private $userId;
    
    private $userFirstName;
    
    private $userLastName;
    
    private $userEmail;
    
    private $userCity;
    
    private $status;
    
    private $errors;
    
    /**
     * Устанавливает параметры сессии полученой с стороннего сервиса авторизации
     *
     * @param string $sessionJson
     */
    public function set(string $sessionJson)
    {
        $session = json_decode($sessionJson, true);
        
        $this->status = $session['status'];
        
        if ($this->status == true) {
            $user = $session['data']['user'];
            
            $this->token = $session['data']['x-access-token'];
            
            $this->userId = $user['id'];
            
            $this->userFirstName = $user['first_name'];
            
            $this->userLastName = $user['last_name'];
            
            $this->userEmail = $user['email'];
            
            $this->userCity = $user['city'];
        }
        
        $this->errors = $session['errors'];
    }
    
    /**
     * Возврашает токен сессии
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
    
    /**
     * id пользователя
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Имя пользователя
     *
     * @return string
     */
    public function getuUserFirstName()
    {
        return $this->userFirstName;
    }
    
    /**
     * Фамилия пользователя
     *
     * @return string
     */
    public function getUserLastName()
    {
        return $this->userLastName;
    }
    
    /**
     * Email пользователя
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }
    
    /**
     * Город пользователя
     *
     * @return string
     */
    public function getUserCity()
    {
        return $this->userCity;
    }
    
    /**
     * Статус сессии
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Ошибки при получении сессии
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
