<?php

namespace App\Addition\Session;

/**
 *
 * @author valentyn
 */
interface SessionInerface
{
    public function set(string $sessionJson);

    public function getToken();

    public function getUserId();

    public function getuUserFirstName();

    public function getUserLastName();

    public function getUserEmail();

    public function getUserCity();

    public function getStatus();

    public function getErrors();
}
