<?php

use Illuminate\Http\Request;


// Группа маршрутов для работы с сервисом REST API версии 1
Route::group(['prefix' => 'v1'], function () {

    // Группа маршрутов для работы c обявлением
    Route::group(['prefix' => 'advert'], function () {

        // Маршрут для добавления обявления
        Route::post('/', [
            'uses' => 'AdvertController@creatAction',
            'middleware' => ['valid.creat.advert', 'authorization']
        ]);

        // Маршрут для получения обявления
        Route::get('/{advert}', [
            'uses' => 'AdvertController@getAction'
        ])->where('advert', '[0-9]+');

        // Маршрут для изменения обявления
        Route::put('/{advert}', [
            'uses' => 'AdvertController@updateAction',
            'middleware' => ['valid.update.advert', 'authorization']
        ])->where('advert', '[0-9]+');

        // Маршрут для удаления обявления
        Route::delete('/{advert}', [
            'uses' => 'AdvertController@deleteAction',
            'middleware' => ['valid.token', 'authorization']
        ])->where('advert', '[0-9]+');

        // Маршрут для добавления обявления в избранное
        Route::post('/{advert}/favorite', [
            'uses' => 'AdvertController@addFavoriteAction',
            'middleware' => ['valid.token', 'authorization']
        ])->where('advert', '[0-9]+');

        // Маршрут для удаления обявления из избранного
        Route::delete('/{advert}/favorite', [
            'uses' => 'AdvertController@deleteFavoriteAction',
            'middleware' => ['valid.token', 'authorization']
        ])->where('advert', '[0-9]+');
        
        // Маршрут для добавления метрики
        Route::post('/{advert}/metrics', [
            'uses' => 'AdvertController@addMetricsAction',
            'middleware' => 'valid.metric'
        ])->where('advert', '[0-9]+');

        // Маршрут для получения метрики
        Route::get('/{advert}/metrics', [
            'uses' => 'AdvertController@getMetricsAction'
        ])->where('advert', '[0-9]+');

        // Маршрут для добавления комментария
        Route::post('/{advert}/comments', [
            'uses' => 'AdvertController@addCommentAction',
            'middleware' => ['valid.comments', 'authorization']
        ])->where('advert', '[0-9]+');
        
        // Маршрут для получения комментариев
        Route::get('/{advert}/comments', [
            'uses' => 'AdvertController@getCommentsAction'
        ])->where('advert', '[0-9]+');
        
        // Маршрут для удаления комментария
        Route::delete('/{advert}/comment/{comment}', [
            'uses' => 'AdvertController@deleteCommentAction',
            'middleware' => ['authorization']
        ])->where(['advert' => '[0-9]+', 'comment' => '[0-9]+']);
    });
    
    // Маршрут для получения все активных обявлений
    Route::get('/adverts', [
        'uses' => 'AdvertController@getAllAction'
    ]);
    
    // Маршрут для получения обявлений по фильтру
    Route::post('/adverts/search', [
        'uses' => 'AdvertController@getFilterAction',
        'middleware' => 'valid.search'
    ]);
    
    // Маршрут для получения избранных обьявлений
    Route::get('/adverts/favorite', [
        'uses' => 'AdvertController@getFavoriteAction',
        'middleware' => ['valid.token', 'authorization']
    ]);
    
    // Маршрут для получения изображения
    Route::any('advert/image/{image}', [
        'uses' => 'AdvertController@getImageAction',
         'as' => 'advert.image'
    ]);

    // Группа маршрутов для работы c категориями
    Route::group(['prefix' => 'categories'], function () {

        // Маршрут для получения списка главных категорий
        Route::get('/', [
            'uses' => 'CategoriesController@getAction'
        ]);

        // Маршрут для получения списка подкатегорий главной категории
        Route::get('/{category}', [
            'uses' => 'CategoriesController@getSubAction'
        ])->where('category', '[0-9]+');
    });
});
